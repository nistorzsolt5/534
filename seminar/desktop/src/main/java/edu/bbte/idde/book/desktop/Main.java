package edu.bbte.idde.book.desktop;

import edu.bbte.idde.book.backend.dao.BookDao;
import edu.bbte.idde.book.backend.dao.memory.BookMemoryDao;
import edu.bbte.idde.book.backend.model.Book;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        BookDao bookDao = new BookMemoryDao();

        Book a = new Book("Pici barmi", "DJ Pici Moriczka", 69);


        bookDao.createBook(a);
        LOG.info(a.toString());

        Book b = new Book("Nagy kinai", "Kiraly Xi", 420);

        bookDao.createBook(b);
        LOG.info("Second book: {}", b);
    }
}
