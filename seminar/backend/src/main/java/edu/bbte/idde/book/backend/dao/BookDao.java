package edu.bbte.idde.book.backend.dao;

import edu.bbte.idde.book.backend.model.Book;

import java.util.Collection;

public interface BookDao {
    Book findById(Long id);
    void createBook(Book book);
    Collection<Book> findAllBook();
}
