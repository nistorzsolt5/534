package edu.bbte.idde.book.backend.dao.memory;

import edu.bbte.idde.book.backend.dao.BookDao;
import edu.bbte.idde.book.backend.model.Book;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class BookMemoryDao implements BookDao {

    private static final Map<Long, Book> ENTITIES = new ConcurrentHashMap<>();
    private static final AtomicLong ID_GENERATOR = new AtomicLong();

    @Override
    public Book findById(Long id) {
        return ENTITIES.get(id);
    }

    @Override
    public void createBook(Book book) {
        Long id = ID_GENERATOR.getAndIncrement();
        book.setId(id);
        ENTITIES.put(id, book);
    }

    @Override
    public Collection<Book> findAllBook() {
        return ENTITIES.values();
    }
}
