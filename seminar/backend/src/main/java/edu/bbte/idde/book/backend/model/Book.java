package edu.bbte.idde.book.backend.model;

public class Book extends BaseEntity{
    private String title;
    private String author;
    private Integer nrOfPages;

    public Book(String title, String author, Integer nrOfPages) {
        this.title = title;
        this.author = author;
        this.nrOfPages = nrOfPages;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getNrOfPages() {
        return nrOfPages;
    }

    public void setNrOfPages(Integer nrOfPages) {
        this.nrOfPages = nrOfPages;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", nrOfPages=" + nrOfPages +
                ", id=" + id +
                '}';
    }
}
